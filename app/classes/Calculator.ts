import CalculatorInterface from '../Interfaces/Calculator.interface';
import * as readline from 'readline';
import * as chalk from 'chalk';
import Operation from './Operation';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

export default class Calculator implements CalculatorInterface {
  history: Operation[] = [];

  private _rl: readline.Interface;

  constructor() {
    this._rl = rl;
    this._rl.on('close', function () {
      process.exit(0);
    });
  }

  // Public Methods

  on(): void {
    this._boot();
    this._run();
  }

  off(): void {
    this._rl.close();
  }

  lastResult(): number {
    return this.history[this.history.length - 1].result;
  }

  lastExpression(): string {
    return this.history[this.history.length - 1].beautify();
  }

  getResult(index: number): number {
    if (index > this.history.length || index < 0) {
      console.log(
        chalk.bgRedBright.bold('ERR:') + ` Nothing in memory slot M${index}`
      );

      return 0;
    }
    return this.history[index].result;
  }
  getHistory(verbose: boolean = false) {
    this.history.forEach((operation, index) => {
      console.log(
        `${chalk.bold.cyanBright('M' + index + ':')} ${
          verbose ? operation.beautify() : operation.result
        }`
      );
    });
  }

  // Private Methods

  /**
   * @method boot
   * @param
   * @description log in console the booting message of the Calculator
   * @returns void
   */
  private _boot(): void {
    console.log(chalk.cyan('Calculator start\n'));
  }

  /**
   * @method run
   * @param
   * @description Launch the Calculator main process and will listen for any input until a `ctrl + c` | `off`
   * @returns void
   */
  private _run(): void {
    this._rl.question('> ', (expression) => {
      this._handler(expression);

      this._run();
    });
  }

  /**
   * @method handler
   * @param expression: string
   * @description Looks at the user input to do the right thing in the calculator, execute an operation, log the history etc...
   * @returns void
   */
  private _handler(expression: string): void {
    if (['off', 'exit'].includes(expression.toLowerCase().trim())) {
      this.off();
    } else if (expression.toLowerCase().trim().startsWith('history')) {
      const splittedExpression: string[] = expression.split(' ');
      if (splittedExpression.length > 1) {
        if (splittedExpression.includes('-verbose')) {
          this.getHistory(true);
        } else if (splittedExpression.includes('-clear')) {
          this.history = [];
        }
      } else {
        this.getHistory();
      }
    } else {
      const operation: Operation = new Operation(expression, this);
      operation.resolve();
      if (!isNaN(operation.result)) {
        this.history.push(operation);
        console.log(operation.beautify());
      }
    }
  }
}
