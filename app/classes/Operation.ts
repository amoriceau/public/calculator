import OperatorsEnum from '../enums/Operator.enum';
import OperationInterface from '../Interfaces/Operation.interface';
import { operators, memory, sub_operation } from '../constants';
import * as chalk from 'chalk';
import Calculator from './Calculator';

export default class Operation implements OperationInterface {
  operation: [number, OperatorsEnum, number];
  calculator: Calculator;
  raw: string;
  parsed: {
    operator: string;
    first_term: number;
    last_term: number;
  };
  result: number;

  constructor(expression: string, calc: Calculator) {
    this.calculator = calc;
    this.raw = expression;
    this.parsed = this._parse(expression);
  }

  // Public methods

  resolve(): void {
    let { first_term, last_term } = this.parsed;
    switch (this.parsed.operator) {
      case '+':
        this.result = first_term + last_term;
        break;
      case '-':
        this.result = first_term - last_term;
        break;
      case '/':
        if (last_term === 0) {
          console.log("Can't divide by 0 !");
          this.result = NaN;
          break;
        }
        this.result = first_term / last_term;
        break;
      case '*':
        this.result = first_term * last_term;
        break;
    }
  }

  beautify(): string {
    return `${chalk.yellowBright(this.parsed.first_term)} ${chalk.red(
      this.parsed.operator
    )} ${chalk.yellowBright(this.parsed.last_term)} => ${chalk.greenBright(
      this.result
    )}`;
  }

  // Private methods

  /**
   * @method parse
   * @param expression: string
   * @description Parse the expression to detect sub-operation or special commands e.g memory commands
   * @returns { operator: string; first_term: number; last_term: number; }
   */
  private _parse(expression: string): {
    operator: string;
    first_term: number;
    last_term: number;
  } {
    let sub = this._subOperations(expression);

    while (sub?.length) {
      let subOperation = new Operation(sub[1] as string, this.calculator);
      subOperation.resolve();
      expression = expression.replace(
        sub[0] as string,
        subOperation.result.toString()
      );
      sub = this._subOperations(expression);
    }

    let first_term_negative: boolean = expression.charAt(0) === '-';
    if (first_term_negative) expression = expression.substring(1);

    let operator: string = expression[expression.search(operators)];
    let terms: string[] = expression.split(operator);

    if (!operator && first_term_negative) {
      expression = '-' + expression;
      operator = expression[expression.search(operators)];
      terms = expression.split(operator);
      first_term_negative = false;
    }

    if (terms[0] === '') {
      if (this.calculator.history.length) {
        terms[0] = this.calculator.lastResult().toString();
      } else {
        terms[0] = '0';
      }
    } else if (terms[0]?.match(memory)) {
      terms[0] = this.calculator.getResult(+terms[0].substring(1)).toString();
    }
    if (terms[1] === '') {
      terms[1] = '0';
    } else if (terms[1]?.match(memory)) {
      terms[1] = this.calculator.getResult(+terms[1].substring(1)).toString();
    }

    return {
      operator,
      first_term: +terms[0] * (first_term_negative ? -1 : 1),
      last_term: +terms[1]
    };
  }

  /**
   * @method subOperations
   * @param expression: string
   * @description Will find the first occurrence of an operation as `(x +|-|\|*  y)` in the given expression in an array or null if no sub operation is found
   * @returns Array | null
   */
  private _subOperations(
    expression: string
  ): (string | number | null)[] | null {
    return sub_operation.exec(expression);
  }
}
