import OperatorEnum from '../enums/Operator.enum';

export default interface OperationInterface {
  operation: [number, OperatorEnum, number];
  raw: string;
  parsed: {
    operator: string;
    first_term: number;
    last_term: number;
  };
  result: number

  /**
   * @method resolve
   * @param
   * @description Calculate the result of the Operation instance
   * @returns void
   */
  resolve(): void;

  /**
   * @method beautify
   * @param
   * @description Enhance the visual of the current operation string
   * @returns void
   */
  beautify(): string;
}
