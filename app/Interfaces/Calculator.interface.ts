import Operation from '../classes/Operation';
export default interface CalculatorInterface {
  history: Array<Operation>;

  /**
   * @method on
   * @param
   * @description Acts like the button 'on' of a real calculator
   * @returns void
   */
  on(): void;

  /**
   * @method off
   * @param
   * @description Acts like the button 'off' of a real calculator
   * @returns void
   */
  off(): void;

  /**
   * @method lastResult
   * @param
   * @description Returns the last result calculated by this Calculator instance
   * @returns number
   */
  lastResult(): number;

  /**
   * @method lastExpression
   * @param
   * @description Returns the last expression stored in this Calculator history
   * @returns string
   */
  lastExpression(): string;

  /**
   * @method getResult
   * @param index:number
   * @description Returns a result stored in the Calculator memory, log an error if the memory slot is empty and returns 0
   * @returns number
   */
  getResult(index: number): number;

  /**
   * @method getHistory
   * @param verbose:boolean @optional
   * @description log in console the memory of the Calculator as M{index}: {result}
   * @returns void
   */
  getHistory(verbose?: boolean): void;
  
}
