export default interface OperatorEnum{
    add: "+";
    minus: "-";
    divide: "/";
    multiply: "*";
}