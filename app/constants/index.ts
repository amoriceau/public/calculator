export const operators = /[+|\-|\/|*|%]/;
export const memory = /^[M|m][0-9]*$/g;
export const commands = {
  off: { cb: 'off', description: 'Turns the calculator off' },
  history: { cb: 'getHistory', description: 'Returns all the results in memory' },
  'history -verbose': { cb: 'getVerboseHistory', description: 'Returns the complete history' },
};
export const sub_operation = /\(([^()]+)\)/g