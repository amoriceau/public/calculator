# calculator
### author: Axel Moriceau

## Description
Calculator is a simple calculator cli tool made in typescript.

## Setup

1. Clone / Download the repo
2. Run `yarn` or `npm install` at the root of the project
3. Run `yarn build && yarn start` or `npm run build && npm run start` to run the project
3. (bis) Run `yarn boot` or `npm run boot` to run the project (the command will use `yarn` so be sure to have it installed on your computer)

## Features
- Basic operations
    - Additions
        - ex: 13 + -3
    - Subtractions
        - ex: -7-3
    - Divisions
        - ex: 6/3
        - Remember you can't divide by zero
    - Multiplications
        - ex: 3 * -4
  

- Nested operations
    - ex 1: (4+3) + 5  -> will print **7 + -5 => 12**
    - ex 2: (6*(45+4))*((2+3)/(7-(6+2)))  -> will print **294 * -5 => -1470**
  

- Operation history
    - Every valid operation will be stored in a local memory unique for every calculator instance
        - e.g: 5+6 is a valid operation, 'test' + 4 is not a valid operation and will not be stored
    - You can access the memory with the command `history` for the list of your result and the location of the result in Memory
        - e.g: 4+5 => 9 --> history --> M0: 9
    - You can use your previous results in any operation (if the result exists else it will be parsed as an invalid operation)
        - e.g: 4+5 => 9 --> M0 + 1 => 10 --> M0 + M1 => 19 and so on
    - You also have access to a more complete history with `history -verbose` which will print the memory key, the operation and the result
    - You can implicitly call the last operation result by typing an operator and an expression
        - e.g: 5+5 => 10 > +5 => 15 > *3 => 45 and so on
  

- Commands
    - any operation is counted as a command
    - `history [-verbose]` -> lists all the operations
    - `history -clear` -> wipe the calculator memory
    - `off` -> turns off the calculator (act like a `ctrl+c`)
    - `exit` -> `off` alias